using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneAnimation : MonoBehaviour
{
    private Animator m_Animator;
    private bool scan = false;
    private bool laser = false;
    private bool facing = true;

    void Start()
    {
        m_Animator = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log("Launching Scanning Animation !");
            scan = true;
            laser = true;
            facing = false;
            m_Animator.SetBool("IsScanning", scan);
            m_Animator.SetBool("IsShowingLaser", laser);
            m_Animator.SetBool("IsFacingObject", facing);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            Debug.Log("Launching Idle Animation !");
            scan = false;
            laser = false;
            facing = true;
            m_Animator.SetBool("IsScanning", scan);
            m_Animator.SetBool("IsShowingLaser", laser);
            m_Animator.SetBool("IsFacingObject", facing);
        }
    }
}
