using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gun : MonoBehaviour
{
    public Transform BulletSpawnPoint;
    public GameObject BulletPrefab;
    public float bulletSpeed = 10.0f;
    public GameObject Shooter;
    private Animator m_Animator;
    public float tempsFinAnim;
    public float tempsShoot;

    private float temps = 0;
    private bool shooting = false;
    private bool aShoote = false;

    private void Start()
    {
        m_Animator = Shooter.GetComponent<Animator>();
    }

    private void Update()
    {
        if (shooting)
        {
            temps += Time.deltaTime;
        }

        if (!shooting && Input.GetKeyDown(KeyCode.F))
        {
            shooting = true;
            aShoote = false;
            m_Animator.SetBool("Shoot", true);
        }

        if (!aShoote && shooting && temps > tempsShoot)
        {
            var bullet = Instantiate(BulletPrefab, BulletSpawnPoint.position, BulletSpawnPoint.rotation);
            bullet.GetComponent<Rigidbody>().velocity = BulletSpawnPoint.forward * bulletSpeed;
            aShoote = true;
        }

        if (shooting && temps > tempsFinAnim)
        {
            temps = 0;
            shooting = false;
            m_Animator.SetBool("Shoot", false);
        }
    }
}
